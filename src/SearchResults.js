import React from "react";
import "./SearchResults.scss";

export default function SearchResults({ movies, clickOnMovie }) {
  return (
    <div className="search-result">
      {movies.slice(0, 8).map((movie) => (
        <ul className="search-result__row" onClick={() => clickOnMovie(movie.title)} key={movie.id}>
          <div>{movie.title}</div>
          <div>
            {movie.vote_average}, {movie.release_date.substr(0, 4)}
          </div>
        </ul>
      ))}
    </div>
  );
}

import SearchMovies from "./SearchMovies";

function App() {
  return (
    <div classname="main-container">
      <SearchMovies />
    </div>
  );
}

export default App;

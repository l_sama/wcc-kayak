import React, { useState, useEffect } from "react";
import axios from "axios";
import "./SearchMovies.scss";
import search from "./assets/img/search.svg";
import movie_white from "./assets/img/movie_white.svg";
import { API_URL, API_KEY, MIN_CHAR } from "./constants";
import SearchResults from "./SearchResults";

export default function SearchMovies() {
  const [movies, setMovies] = useState([]);
  const [movieTitle, setMovieTitle] = useState("");
  const [query, setQuery] = useState("");
  const [error, setError] = useState(false);
  const [searchResults, showSearchResults] = useState(false);
  const [searchClicked, isSearchClicked] = useState(false);

  useEffect(() => {
    const searchMovies = () => {
      if (query.length > MIN_CHAR) {
        axios
          .get(`${API_URL}?api_key=${API_KEY}&language=en-US&query=${query}`)
          .then((response) => {
            setMovies(response.data.results);
            showSearchResults(true);
          })
          .catch((error) => setError(true));
      } else {
        setMovies([]);
      }
    };
    searchMovies();
  }, [query]);

  const handleInputChange = (e) => {
    e.preventDefault();
    setQuery(e.target.value);
  };

  const onSearchInputClick = () => {
    isSearchClicked(true);
  };

  const handleSelectedMovie = (variable) => {
    setMovieTitle(variable);
    showSearchResults(false);
    isSearchClicked(false);
  };

  return (
    <>
      <div className="wrapper">
        {searchClicked ? (
          <div className="search-clicked">
            <img className="movie-icon" src={movie_white} alt="movie" />
            <input className="input-clicked" type="text" value={query} onChange={handleInputChange} />
          </div>
        ) : (
          <div className="search-not-clicked">
            <img className="movie-icon" src={movie_white} alt="movie" />
            <input
              className="input-not-clicked"
              placeholder="Enter movie name"
              type="text"
              onClick={onSearchInputClick}
              value={movieTitle}
            />
            <img className="search-icon" src={search} alt="search" />
          </div>
        )}
      </div>
      {searchResults && <SearchResults movies={movies} clickOnMovie={handleSelectedMovie} />}
    </>
  );
}
